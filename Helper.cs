﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Collections.ObjectModel;


using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Linq;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml;
using System.Security.Cryptography.Xml;
using World;

namespace GitLabHelperGUI
{
    public class Helper
    {
        public Helper(bool aScan = false)
        {
            fClient = new HttpClient();
            fClient.DefaultRequestHeaders.Add("Private-Token", SimpleIni.Ini.Value("GitLabApiToken"));
            // override scan option if no scan data is available
            if (!File.Exists(DataFileName))
            {
                if (!aScan)
                    Debug.WriteLine(">> Overriding /scan option because no cached data found...");
                aScan = true;
            }
            if (aScan)
            {
                fProjectsHTTP = new Dictionary<string, GitLabProject>();
                //Rescan(null);
            }
            else
                Reload(/*null*/);
        }

        public void Rescan(Action<int, int, string> aSetProgress)
        {
            // scan all groups to build cache
            lock (fProjectsHTTP)
            {
                fProjectsHTTP.Clear();
                fProjectsSSH.Clear();
            }
            lock (fSources)
                fSources.Clear();
            ReadAllProjects_nl(SimpleIni.Ini.Value("GitLabGroups").Split("|"), aSetProgress, "Reading all projects (1/5)");
            ReadAllBranchesAndSubmodules_nl(aSetProgress, "Reading branches and submodules (2/5)");
            ProcessAllSubmodules(aSetProgress, "Processing submodules (3/5)");
            UpdateAllJobs(aSetProgress, "Updating jobs (4/5)");
            SaveData(aSetProgress, "Saving");
            lock(fSources)
            {
                Debug.WriteLine("sources");
                foreach (var src in fSources)
                    Debug.WriteLine((IsValidProjectSource(src.Key) ? "   " : "## ") + src.Key.ToString() + " (" + Convert.ToString(src.Key, 2) + "): " + src.Value.ToString());
            }
        }

        public bool IsValidProjectSource(int aSource) { return fValidSources.Contains(aSource); }

        private readonly List<int> fValidSources = new() { 0, 1, 2, 3, 4, 16, 32, 33, 34, 35, 49, 51, 64, 65, 68, 69, 128, 129, 160, 161, 256, 512, 1024, 2048, 2056, 3072 };

        public void RescanProject(Action<int, int, string> aSetProgress, string aProjectURL)
        {
            if (fProjectsHTTP.TryGetValue(aProjectURL, out GitLabProject project) || fProjectsSSH.TryGetValue(aProjectURL, out project))
            {
                aSetProgress?.Invoke(0, 3, "Scanning");
                ReadAllBranchesAndSubmodulesForProject(project);
                aSetProgress?.Invoke(1, 3, "Scanning");
                ProcessAllSubmodulesForProject(project);
                aSetProgress?.Invoke(2, 3, "Scanning");
                UpdateAllJobsForProject(project);
                aSetProgress?.Invoke(3, 3, "Scanning");
                SaveData(aSetProgress, "Saving");
                foreach (var branch in project.Branches)
                    Debug.WriteLine((IsValidProjectSource(branch.Value.Source) ? "   " : "## ") + aProjectURL + "#" + branch.Key + " (" + Convert.ToString(branch.Value.Source, 2) + "): " + branch.Value.Source.ToString());
            }
        }

        public void Reload(/*Action<int, int, string> aSetProgress*/)
        {
            // read projects and submodules from cached scan
            fProjectsHTTP?.Clear();
            fProjectsHTTP = JsonConvert.DeserializeObject<Dictionary<string, GitLabProject>>(
                File.ReadAllText(DataFileName), 
                new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate });
            // add ssh link lookups
            fProjectsSSH.Clear();
            foreach (var project in fProjectsHTTP)
                fProjectsSSH[project.Value.URLSSH] = project.Value;
        }

        private void ReadAllProjects_nl(string[] aGroups, Action<int, int, string> aSetProgress, string aStage)
        {
            int i = 0;
            aSetProgress?.Invoke(i, aGroups.Length, aStage);
            Parallel.ForEach(aGroups, (group) =>
            {
                var jProjects = RequestGet("groups/" + group.Replace("/", "%2F") + "/projects?include_subgroups=true&simple=true&per_page=100");
                if (fProjectsHTTP != null && jProjects != null)
                    foreach (var jProject in jProjects)
                    {
                        lock (fProjectsHTTP)
                        {
                            AddProject(new GitLabProject(jProject, true));
                        }
                    }
                Interlocked.Increment(ref i);
                aSetProgress?.Invoke(i, aGroups.Length, aStage);
            });
        }

        private void AddProject(GitLabProject aProject)
        {
            if (aProject.URLHttp.Length > 0)
                fProjectsHTTP[aProject.URLHttp] = aProject;
            if (aProject.URLSSH.Length > 0)
                fProjectsSSH[aProject.URLSSH] = aProject;
        }

        private void ReadAllBranchesAndSubmodules_nl(Action<int, int, string> aSetProgress, string aStage)
        {
            int i = 0;
            aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
            lock (fProjectsHTTP)
            {
                Parallel.ForEach(fProjectsHTTP, (project) =>
                {
                    ReadAllBranchesAndSubmodulesForProject(project.Value);
                    Interlocked.Increment(ref i);
                    aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
                });
            }
        }

        private void ReadAllBranchesAndSubmodulesForProject(GitLabProject aProject)
        {
            // read branches
            var jBranches = RequestGet("projects/" + aProject.ID.ToString() + "/repository/branches");
            aProject.Branches.Clear();
            foreach (var jBranch in jBranches)
            {
                var branch = new GitLabBranch(jBranch);
                aProject.Branches.Add(branch.Name, branch);
                // scan sub modules
                ReadBranchAllSubmodules(aProject, branch);
            }
        }

        private void ReadBranchAllSubmodules(GitLabProject aProject, GitLabBranch aBranch)
        {
            var taskGitModules = new Task(() =>
                {
                    try
                    {
                        var content = RequestProjectFile(aProject.ID, aBranch.Name, ".gitmodules");
                        if (content != null)
                        {
                            var lines = content.Split("\n");
                            if (lines.Length > 0)
                            {
                                string smName = "";
                                string smPath = "";
                                string smUrl = "";
                                string smBranch = "";
                                string jSubmoduleCommit;
                                foreach (var line in lines)
                                {
                                    if (line.Trim().StartsWith("["))
                                    {
                                        var split = line.Split("\"");
                                        if (split.Length > 1)
                                        {
                                            // save last branch
                                            if (smPath.Length > 0)
                                            {
                                                var JCommit = RequestGet("projects/" + aProject.ID.ToString() + "/repository/files/" + (smPath.Replace("/", "%2F")) + "?ref=" + aBranch.Name);
                                                jSubmoduleCommit = JCommit != null ? JCommit["blob_id"].Value<string>() : "";
                                                aBranch.Submodules[smName] = new GitLabSubmodule() { Name = smName, Branch = smBranch, Path = smPath, URL = smUrl, Commit = jSubmoduleCommit };
                                            }
                                            smName = split[1].Trim();
                                            smPath = "";
                                            smUrl = "";
                                            smBranch = "";
                                            jSubmoduleCommit = "";
                                        }
                                    }
                                    else
                                    {
                                        var split = line.Split("=");
                                        if (split.Length == 2)
                                        {
                                            var item = split[0].Trim();
                                            if (item.Equals("path", StringComparison.OrdinalIgnoreCase))
                                                smPath = split[1].Trim();
                                            else if (item.Equals("url", StringComparison.OrdinalIgnoreCase))
                                                smUrl = split[1].Trim();
                                            else if (item.Equals("branch", StringComparison.OrdinalIgnoreCase))
                                                smBranch = split[1].Trim();
                                            else
                                                Debug.WriteLine(">> unknown field in gitsubmodules: " + item);
                                        }
                                    }
                                }
                                if (smName.Length > 0 && smPath.Length > 0)
                                {
                                    var JCommit = RequestGet("projects/" + aProject.ID.ToString() + "/repository/files/" + (smPath.Replace("/", "%2F")) + "?ref=" + aBranch.Name);
                                    jSubmoduleCommit = JCommit != null ? JCommit["blob_id"].Value<string>() : "";
                                    aBranch.Submodules[smName] = new GitLabSubmodule() { Name = smName, Branch = smBranch, Path = smPath, URL = smUrl, Commit = jSubmoduleCommit };
                                }
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        Debug.WriteLine("## " + aProject.Path + "#" + aBranch.Name + ": " + ".gitmodules" + ": " + e.Message);
                    }
                });
            taskGitModules.Start();
            var taskDetectSource = new Task(() =>
                {
                    aBranch.Source = 0;
                    bool transform = false;
                    bool packages = false;
                    try
                    {
                        var jTreeSibModulesFiles = RequestGet("projects/" + aProject.ID.ToString() + "/repository/tree?ref=" + aBranch.Name + "&per_page=50", false);
                        if (jTreeSibModulesFiles is JArray jFiles)
                        {
                            foreach (var fFile in jFiles)
                            {
                                var fileName = fFile["name"].Value<string>();
                                var ext = Path.GetExtension(fileName).ToLower();
                                switch(ext)
                                {
                                    case ".dpr":
                                        aBranch.Source |= srcDelphi | srcWindows;
                                        aBranch.ProjectFiles.Add(fileName);
                                        break;
                                    case ".dproj":
                                        aBranch.Source |= srcDelphi | srcWindows;
                                        aBranch.SolutionFiles.Add(fileName);
                                        aBranch.ProjectVersion = ReadAndParseDProjProjectVersion(aProject.ID, aBranch.Name, fileName);
                                        break;
                                    case ".pas":
                                        aBranch.Source |= srcDelphi;
                                        break;
                                    case ".js":
                                        aBranch.Source |= srcJavascript;
                                        break;
                                    case ".njsproj":
                                        aBranch.Source |= srcNodeJS | srcJavascript;
                                        aBranch.ProjectFiles.Add(fileName);
                                        break;
                                    case ".csproj":
                                        aBranch.Source |= srcCsharp;
                                        aBranch.ProjectFiles.Add(fileName);
                                        aBranch.Source |= ReadAndParseCSProj(aProject.ID, aBranch.Name, fileName, ref transform, ref packages);
                                        break;
                                    case ".cs":
                                        aBranch.Source |= srcCsharp;
                                        break;
                                    case ".xaml":
                                        aBranch.Source |= srcCsharp | srcWindows;
                                        break;
                                    case ".vcxproj":
                                        aBranch.Source |= srcCpp;
                                        aBranch.ProjectFiles.Add(fileName);
                                        aBranch.Source |= ReadAndParseVCXProj(aProject.ID, aBranch.Name, fileName, ref packages);
                                        break;
                                    case ".vcproj":
                                        aBranch.Source |= srcCpp;
                                        aBranch.ProjectFiles.Add(fileName);
                                        aBranch.Source |= ReadAndParseVCProj(aProject.ID, aBranch.Name, fileName, ref packages);
                                        break;
                                    case ".h":
                                    case ".hpp":
                                    case ".cpp":
                                        aBranch.Source |= srcCpp;
                                        break;
                                    case ".py":
                                        aBranch.Source |= srcPython;
                                        break;
                                    case ".java":
                                        aBranch.Source |= srcJava;
                                        break;
                                    case ".html":
                                    case ".htm":
                                        aBranch.Source |= srcHtml;
                                        break;
                                    case ".sln":
                                        aBranch.Source |= ReadAndParseSolution(aProject.ID, aBranch.Name, fileName, aBranch.ProjectFiles, ref transform, ref packages);
                                        aBranch.SolutionFiles.Add(fileName);
                                        break;
                                }
                            }
                        }
                        aBranch.Transform = transform;
                        aBranch.Packages = packages;
                        if (aBranch.Source == 0)
                            Debug.WriteLine(">> " + aProject.Path + "#" + aBranch.Name + ": " + aBranch.Source.ToString());
                    }
                    catch(Exception e)
                    {
                        Debug.WriteLine("## " + aProject.Path + "#" + aBranch.Name + ": " + aBranch.Source.ToString() + ": " + e.Message);
                    }
                    lock (fSources)
                    {
                        fSources[aBranch.Source] = fSources.TryGetValue(aBranch.Source, out int cnt) ? cnt + 1 : 1;
                    }
                });
            taskDetectSource.Start();
            Task.WaitAll(taskGitModules, taskDetectSource);
        }

        private readonly Dictionary<int, int> fSources = new();

        private readonly string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());

        private int ReadAndParseCSProj(int aProjectID, string aBranch, string aFileName, ref bool aTransform, ref bool aPackages)
        {
            if (PackagesConfigExists(aProjectID, aBranch, Path.GetDirectoryName(aFileName)))
                aPackages = true;
            var content = RequestProjectFile(aProjectID, aBranch, aFileName);
            try
            {
                if (content != null)
                {
                    if (content.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
                        content = content.Remove(0, _byteOrderMarkUtf8.Length);
                    var project = XElement.Parse(content);
                    var targetFramework = GetXMLValueInDescendants(project, "TargetFramework");
                    if (targetFramework.Length > 0)
                    {
                        Debug.WriteLine("      C# " + aProjectID.ToString() + "#" + aBranch + ": " + targetFramework + " in " + aFileName);
                        if (targetFramework.StartsWith("netcoreapp", StringComparison.OrdinalIgnoreCase))
                        {
                            if (GetXMLValueInDescendants(project, "TransformOnBuild").Equals("true"))
                                aTransform = true;
                            return srcDotNetCore;
                        }
                        else
                            return 0;
                    }
                    else
                    {
                        targetFramework = GetXMLValueInDescendants(project, "{http://schemas.microsoft.com/developer/msbuild/2003}TargetFrameworkVersion");
                        if (targetFramework.Length > 0)
                        {
                            Debug.WriteLine("      C# " + aProjectID.ToString() + "#" + aBranch + ": " + targetFramework + " in " + aFileName);
                            return srcWindows;
                        }
                        else
                            return 0;
                    }
                }
                else
                {
                    Debug.WriteLine("      C# " + aProjectID.ToString() + "#" + aBranch + ": project file " + aFileName + " not found");
                    return 0;
                }
            }
            catch(Exception e)
            {
                Debug.WriteLine("## exeption parsing C# " + aProjectID.ToString() + "#" + aBranch + " file " + aFileName + ": " + e.Message);
                return 0;
            }
        }

        private string ReadAndParseDProjProjectVersion(int aProjectID, string aBranch, string aFileName)
        {
            var content = RequestProjectFile(aProjectID, aBranch, aFileName);
            try
            {
                if (content != null)
                {
                    if (content.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
                        content = content.Remove(0, _byteOrderMarkUtf8.Length);
                    var project = XElement.Parse(content);
                    var projectVersion = GetXMLValueInDescendants(project, "{http://schemas.microsoft.com/developer/msbuild/2003}ProjectVersion");
                    Debug.WriteLine("      Delphi " + aProjectID.ToString() + "#" + aBranch + ": " + projectVersion + " in " + aFileName);
                    return projectVersion;
                }
                else
                    return "";
            }
            catch (Exception e)
            {
                Debug.WriteLine("## exeption parsing Delphi project " + aProjectID.ToString() + "#" + aBranch + " file " + aFileName + ": " + e.Message);
                return "";
            }
        }

        private static string GetXMLValueInDescendants(XElement aElement, string aDescendant, string aDefault = "")
        {
            var descendants = aElement.Descendants(aDescendant);
            return descendants.Any() ? (descendants.First().Value ?? aDefault) : aDefault;
        }

        private int ReadAndParseVCXProj(int aProjectID, string aBranch, string aFileName, ref bool aPackages)
        {
            var source = 0;
            if (PackagesConfigExists(aProjectID, aBranch, Path.GetDirectoryName(aFileName)))
                aPackages = true;
            var content = RequestProjectFile(aProjectID, aBranch, aFileName);
            try
            {
                if (content != null)
                {
                    string WindowsTargetPlatformVersion = "?";
                    if (content.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
                        content = content.Remove(0, _byteOrderMarkUtf8.Length);
                    var project = XElement.Parse(content);
                    var windowsTargetPlatformVersionElements = project.Descendants("{http://schemas.microsoft.com/developer/msbuild/2003}WindowsTargetPlatformVersion");
                    if (windowsTargetPlatformVersionElements.Any())
                    {
                        WindowsTargetPlatformVersion = windowsTargetPlatformVersionElements.First().Value;
                        source |= srcWindows;
                    }
                    else
                    {
                        var applicationtypeElements = project.Descendants("{http://schemas.microsoft.com/developer/msbuild/2003}ApplicationType");
                        if (applicationtypeElements.Any() && applicationtypeElements.First().Value.Equals("linux", StringComparison.OrdinalIgnoreCase))
                            source |= srcLinux;
                        else
                        {
                            var keyWordElements = project.Descendants("{http://schemas.microsoft.com/developer/msbuild/2003}Keyword");

                            foreach (var keyWordElement in keyWordElements)
                            {
                                if (keyWordElement.Value.Equals("Win32Proj", StringComparison.OrdinalIgnoreCase))
                                    return source |= srcWindows;
                                if (keyWordElement.Value.Equals("Linux", StringComparison.OrdinalIgnoreCase))
                                    return source |= srcLinux;
                            }
                        }
                    }
                    Debug.WriteLine("      C++ " + aProjectID.ToString() + "#" + aBranch + ": " + WindowsTargetPlatformVersion + " in " + aFileName);
                    if (source == 0)
                        source |= srcWindows;
                    // test for cuda
                    var cudaCompileElements = project.Descendants("{http://schemas.microsoft.com/developer/msbuild/2003}CudaCompile");
                    if (cudaCompileElements.Any())
                        source |= srcCuda;
                }
                else
                    Debug.WriteLine(">> missing project file parsing C++ " + aProjectID.ToString() + "#" + aBranch + " file " + aFileName);
                return source;
            }
            catch (Exception e)
            {
                Debug.WriteLine("## exeption parsing C++ " + aProjectID.ToString() + "#" + aBranch + " file " + aFileName + ": " + e.Message);
                return 0;
            }
        }
        
        private int ReadAndParseVCProj(int aProjectID, string aBranch, string aFileName, ref bool aPackages)
        {
            var source = srcWindows; //  old style projects are always windows !?
            if (PackagesConfigExists(aProjectID, aBranch, Path.GetDirectoryName(aFileName)))
                aPackages = true;
            var content = RequestProjectFile(aProjectID, aBranch, aFileName);
            try
            {
                if (content.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
                    content = content.Remove(0, _byteOrderMarkUtf8.Length);
                var project = XElement.Parse(content);
                var tools = project.Descendants("Tool").Where(e => e.Attributes().Any(a => a.Name == "Name" && (a.Value == "Cudart Build Rule" || a.Value == "CUDA Build Rule")));
                if (tools.Any())
                    source |= srcCuda;
            }
            catch (Exception e)
            {
                Debug.WriteLine("## exeption parsing C++ " + aProjectID.ToString() + "#" + aBranch + " file " + aFileName + ": " + e.Message);
                return 0;
            }
            return source;
        }

        public bool PackagesConfigExists(int aProjectID, string aBranch, string aFolder)
        {
            return RequestProjectFileExists(aProjectID, aBranch, Path.Combine(aFolder, "packages.config"));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0057:Use range operator", Justification = "Backwards compatible CS language version")]
        private int ReadAndParseSolution(int aProjectID, string aBranch, string aFileName, List<string> aProjectFiles, ref bool aTransform, ref bool aPackages)
        {
            int source = 0;
            var lines = RequestProjectFile(aProjectID, aBranch, aFileName).Split("\n");
            foreach(var line in lines)
            {
                if (line.StartsWith("Project(\"{"))
                {
                    var projectSplit = line.Split("=");
                    if (projectSplit.Length==2)
                    {
                        var namePathID = projectSplit[1].Split(","); 
                        if (namePathID.Length==3)
                        {
                            // we a valid project line
                            // trim and remove first quote char
                            var path = namePathID[1].Trim().Substring(1);
                            // remove last quote char
                            path = path.Substring(0, path.Length - 1);
                            // only process items in sub dirs (others are already parsed)
                            if (path.Contains('\\'))
                            {
                                // replace \ to url encoded path char %2F
                                var fileName = path.Replace("\\", "%2F");
                                var ext = Path.GetExtension(path).ToLower();
                                switch (ext)
                                {
                                    case ".csproj":
                                        source |= srcCsharp;
                                        aProjectFiles.Add(path);
                                        source |= ReadAndParseCSProj(aProjectID, aBranch, fileName, ref aTransform, ref aPackages);
                                        break;
                                    case ".vcxproj":
                                        source |= srcCpp;
                                        aProjectFiles.Add(path);
                                        source |= ReadAndParseVCXProj(aProjectID, aBranch, fileName, ref aPackages);
                                        break;
                                    case ".vcproj":
                                        source |= srcCpp;
                                        aProjectFiles.Add(path);
                                        source |= ReadAndParseVCProj(aProjectID, aBranch, fileName, ref aPackages);
                                        break;
                                    case ".njsproj":
                                        source |= srcNodeJS | srcJavascript;
                                        aProjectFiles.Add(path);
                                        break;
                                    default:
                                        aProjectFiles.Add(path);
                                        Debug.WriteLine("## unknown project type in " + aProjectID.ToString() + "#" + aBranch + " " + aFileName + ": " + path);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return source;
        }

        public string RequestProjectFile(int aProjectID, string aBranch, string aFileName)
        {
            var jFile = RequestGet("projects/" + aProjectID.ToString() + "/repository/files/" + aFileName.Replace("/", "%2F") + "?ref=" + aBranch, false);
            return jFile != null ? Encoding.UTF8.GetString(jFile["content"].ToObject<byte[]>(JsonSerializer.Create(new JsonSerializerSettings()))) : null;
        }

        public bool RequestProjectFileExists(int aProjectID, string aBranch, string aFileName)
        {
            return RequestGet("projects/" + aProjectID.ToString() + "/repository/files/" + aFileName.Replace("/", "%2F") + "?ref=" + aBranch, false) != null;
        }

        // platform
        public const int srcWindows = 1 << 0;
        public const int srcLinux = 1 << 1;
        // framework
        public const int srcDotNetCore = 1 << 2;
        public const int srcNodeJS = 1 << 3; // also implies javascripts
        // feature
        public const int srcCuda = 1 << 4;
        // language
        public const int srcCpp = 1 << 5;
        public const int srcCsharp = 1 << 6;
        public const int srcDelphi = 1 << 7;
        public const int srcPython = 1 << 8;
        public const int srcJava = 1 << 9;
        public const int srcHtml = 1 << 10;
        public const int srcJavascript = 1 << 11;
        //public const int srcMacOS = 1 << 12;

        /*
           32    (00000100000): 28	5		c++						    32
           64 	 (00001000000): 5	6		c#						    64
           34 	 (00000100010): 9	1+5		* linux + c++				2+32    -
           33 	 (00000100001): 26	0+5		* windows + c++			    1+32    x
           68 	 (00001000100): 5	2+6		* dotnet + c#				4+64    x
           65 	 (00001000001): 10	0+6		* windows + c#			    1+64    x
           256 	 (00100000000): 8	8		python					    256
           0 	 (00000000000): 32	x								    0
           128 	 (00010000000): 20	7		delphi					    128
           160 	 (00010100000): 1	5+7		? c++ + delphi			    32+128
           129 	 (00010000001): 25	0+7		* windows + delphi		    1+128   -
           1024  (10000000000): 1	10		html					    1024
           8 	 (00000001000): 5	3		* nodejs					8
           512 	 (01000000000): 2	9		java					    512
           49 	 (00000110001): 20	0+4+5	* windows + cuda + c++	    1+16+32
           161 	 (00010100001): 1	1+5+7	? windows + c++ + delphi	1+32+128
           1032  (10000001000): 0    3+10   nodejs+html                 8+1024

        ## 2056 (100000001000): 2           nodejs+javascript
        ## 2048 (100000000000): 1           javascript
        ## 3072 (110000000000): 1           htlm+javascript

        */



        public void UpdateAllJobs(Action<int, int, string> aSetProgress)
        {
            UpdateAllJobs(aSetProgress, "Updating jobs");
        }

        public void UpdateAllJobs(Action<int, int, string> aSetProgress, string aStage)
        {
            int i = 0;
            lock (fProjectsHTTP)
            {
                aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
                Parallel.ForEach(fProjectsHTTP, (project) =>
                {
                    UpdateAllJobsForProject(project.Value);
                    Interlocked.Increment(ref i);
                    aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
                });
            }
        }

        public void UpdateAllJobsForProject(GitLabProject aProject)
        {
            if (aProject.HasJobs)
            {
                // remove all jobs from all branches
                aProject.HasJobs = false;
                foreach (var branch in aProject.Branches)
                    branch.Value.Jobs.Clear();
                // rescan jobs for project and add to branches
                var jJobs = RequestGet("projects/" + aProject.ID.ToString() + "/jobs");
                if (jJobs != null)
                {
                    foreach (var jJob in jJobs)
                    {
                        string jBranch = jJob["ref"].Value<string>();
                        if (aProject.Branches.TryGetValue(jBranch, out var branch))
                        {
                            var job = new GitLabJob(jJob);
                            // only store first job (latest) for stage: they are retrieved in descending order
                            if (!branch.Jobs.ContainsKey(job.Name))
                                branch.Jobs[job.Name] = job;
                        }
                        aProject.HasJobs = true;
                    }
                }
            }
        }

        public void UpdateAllSubmodules(Action<int, int, string> aSetProgress)
        {
            UpdateAllSubmodules(aSetProgress, "Updating submodules");
        }

        public void UpdateAllSubmodules(Action<int, int, string> aSetProgress, string aStage)
        {
            int i = 0;
            lock (fProjectsHTTP)
            {
                aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
                Parallel.ForEach(fProjectsHTTP, (project) =>
                {
                    foreach (var branch in project.Value.Branches)
                    {
                        Parallel.ForEach(branch.Value.Submodules, (submodule) =>
                        {
                            ReadSubmodule(project.Value.ID, branch.Key, submodule.Value, project.Value.URLHttp);
                        });
                    }
                    Interlocked.Increment(ref i);
                    aSetProgress?.Invoke(i, fProjectsHTTP.Count, aStage);
                });
            }
        }

        private void ReadSubmodule(int aProjectID, string aBranch, GitLabSubmodule aSubmodule, string aProjectURL)
        {
            var JCommit = RequestGet("projects/" + aProjectID.ToString() + "/repository/files/" + (aSubmodule.Path.Replace("/", "%2F")) + "?ref=" + aBranch);
            aSubmodule.Commit = JCommit != null ? JCommit["blob_id"].Value<string>() : "";
            UpdateSubmoduleCommit(aProjectID, aBranch, aSubmodule, aProjectURL);
        }

        private bool UpdateSubmoduleCommit(int aProjectID, string aBranch, GitLabSubmodule aSubmodule, string aProjectURL)
        {
            GitLabProject smProject;
            lock (fProjectsHTTP)
            {
                if (!fProjectsHTTP.TryGetValue(aSubmodule.URL, out smProject) && !fProjectsSSH.TryGetValue(aSubmodule.URL, out smProject))
                {
                    string path = "";
                    var url = aSubmodule.URL;
                    try
                    {
                        if (url.StartsWith("git@ci.tno.nl:", StringComparison.OrdinalIgnoreCase))
                        {
                            path = url[("git@ci.tno.nl:").Length..];
                        }
                        else
                        {
                            if (url.StartsWith(".."))
                                url = MakeAbsoluteURIPath(aProjectURL, url);
                            var uri = new Uri(aSubmodule.URL);
                            path = uri.PathAndQuery;
                            if (path.StartsWith("/"))
                                path = path[1..];
                            if (path.StartsWith("gitlab/"))
                                path = path[("gitlab/").Length..];
                        }
                        if (path.EndsWith(".git"))
                            path = path[..^(".git").Length];
                        smProject = ReadProject(path);
                        if (smProject != null)
                        {
                            AddProject(smProject);
                            ReadAllBranchesAndSubmodulesForProject(smProject);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("## UpdateSubmoduleCommit " + aSubmodule.URL + " exception: " + e.Message);
                    }
                }
            }
            if (smProject != null)
            {
                smProject.IsSubmodule = true;
                if (!smProject.Branches.TryGetValue(aSubmodule.Branch, out var smBranch))
                {
                    Debug.WriteLine(">> could not find sub module " + aSubmodule.Name + " branch " + aSubmodule.Branch + " to update for " + ProjectIDToURL(aProjectID) + "#" + aBranch + "$" + aSubmodule.Path + ": " + aSubmodule.URL);
                    smBranch = null;
                }
                aSubmodule.HeadCommit = smBranch?.Commit ?? "";
                aSubmodule.IsOnHead = aSubmodule.Commit.Length == 0 || aSubmodule.Commit == (smBranch?.Commit ?? "");
                aSubmodule.IsValid = true;
                return true;
            }
            else
            {
                Debug.WriteLine(">> could not find sub module " + aSubmodule.Name + " branch " + aSubmodule.Branch + " to update for " + ProjectIDToURL(aProjectID) + "#" + aBranch + "$" + aSubmodule.Path + ": " + aSubmodule.URL);
                aSubmodule.IsValid = false;
                return false;
            }
        }

        private GitLabProject ReadProject(string aProjectIDOrURLEncodedPath)
        {
            var jProject = RequestGet("projects/" + aProjectIDOrURLEncodedPath.Replace("/", "%2F"));
            return jProject != null ? new GitLabProject(jProject, true) : null;
        }

        private string ProjectIDToURL(int aProjectID)
        {
            foreach(var project in fProjectsHTTP)
            {
                if (project.Value.ID == aProjectID)
                    return project.Key;
            }
            return aProjectID.ToString();
        }

        private void ProcessAllSubmodules(Action<int, int, string> aSetProgress, string aStage)
        {
            int i = 0;
            List<GitLabProject> projects = null;
            lock (fProjectsHTTP)
            {
                projects = fProjectsHTTP.Values.ToList();
            }
            aSetProgress?.Invoke(i, projects.Count, aStage);
            Parallel.ForEach(projects, (project) =>
            {
                ProcessAllSubmodulesForProject(project); // can add projects
                    Interlocked.Increment(ref i);
                aSetProgress?.Invoke(i, projects.Count, aStage);
            });
        }

        private void ProcessAllSubmodulesForProject(GitLabProject aProject)
        {
            foreach (var branch in aProject.Branches.Values.ToList())
            {
                foreach (var subModule in branch.Submodules.Values.ToList())
                {
                    if (subModule.URL.StartsWith(".."))
                        subModule.URL = MakeAbsoluteURIPath(aProject.WEBURL, subModule.URL);
                    if (subModule.Branch.Length == 0)
                        subModule.Branch = "master";
                    UpdateSubmoduleCommit(aProject.ID, branch.Name, subModule, aProject.URLHttp);
                }
            }
        }

        public bool PutSubmoduleOnCommitInGitlab(int aProjectID, string aBranch, string aSubmodulePath, string aCommit)
        {
            var response = RequestPut(
                "projects/" + aProjectID.ToString() + "/repository/submodules/" + (aSubmodulePath.Replace("/", "%2F")),
                new List<KeyValuePair<string, string>>() { 
                    new KeyValuePair<string, string>("branch", aBranch), 
                    new KeyValuePair<string, string>("commit_sha", aCommit) });
            return response != null;
        }

        public JToken PostCommit(int aProjectID, JToken aCommit)
        {
            return RequestPost("projects/" + aProjectID.ToString() + "/repository/commits", aCommit);
        }

        public JToken RequestPipelines(int aProjectID, string aBranch = "")
        {
            return RequestGet("projects/" + aProjectID.ToString() + "/pipelines?per_page=100" + (aBranch.Length > 0 ? "&ref=" + aBranch : ""));
        }

        public JToken RequestPipelineDelete(int aProjectID, int aPipelineID)
        {
            //DELETE / projects /:id / pipelines /:pipeline_id
            return RequestDelete("projects/" + aProjectID.ToString() + "/pipelines/" + aPipelineID.ToString());
        }

        public bool SetSubmoduleCommitOnHead(int aProjectID, string aBranch, string aSubmodulePath)
        {
            lock (fProjectsHTTP)
            {
                foreach (var project in fProjectsHTTP)
                {
                    if (project.Value.ID == aProjectID)
                    {
                        if (project.Value.Branches.TryGetValue(aBranch, out var branch))
                        {
                            if (branch.Submodules.TryGetValue(aSubmodulePath, out var submodule))
                            {
                                submodule.Commit = submodule.HeadCommit;
                                submodule.IsOnHead = true;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public void SaveData(Action<int, int, string> aSetProgress, string aStage)
        {
            aSetProgress?.Invoke(0, 2, aStage);
            Debug.WriteLine("Saving to " + DataFileName + "...");
            string json;
            lock (fProjectsHTTP)
            {
                json = JsonConvert.SerializeObject(
                    fProjectsHTTP, Newtonsoft.Json.Formatting.Indented, 
                    new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate });
            }
            aSetProgress?.Invoke(1, 2, aStage);
            File.WriteAllText(DataFileName, json);
            aSetProgress?.Invoke(2, 2, aStage);
        }

        public static string MakeAbsoluteURIPath(string aBaseURIPath, string aRelativeURIPath)
        {
            var baseURI = new Uri(IncludeTrailingURIPathSeparator(aBaseURIPath));
            var myUri = new Uri(baseURI, aRelativeURIPath);
            return myUri.AbsoluteUri;
        }

        public static string MakeRelativeURIPath(string aBaseURIPath, string aAbsoluteURIPath)
        {
            if (aAbsoluteURIPath.StartsWith(".."))
                return aAbsoluteURIPath;
            else
            {
                var baseURI = new Uri(IncludeTrailingURIPathSeparator(aBaseURIPath));
                var myUri = new Uri(aAbsoluteURIPath);
                return baseURI.MakeRelativeUri(myUri).ToString();
            }
        }

        private static string IncludeTrailingURIPathSeparator(string aURIPath)
        {
            return aURIPath.Length > 0 && !aURIPath.EndsWith("/") ? aURIPath + "/" : aURIPath;
        }

        private JToken RequestGet(string aRelativeURL, bool aLogError = true)
        {
            var response = fClient.GetAsync(SimpleIni.Ini.Value("GitLabApiPath") + "/" + aRelativeURL).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;
                string responseString = responseContent.ReadAsStringAsync().Result;
                return JToken.Parse(responseString);
            }
            else
            {
                if (aLogError)
                    Debug.WriteLine("## error in GET request on " + aRelativeURL + ": " + response.StatusCode);
                return null;
            }
        }

        private JToken RequestPut(string aRelativeURL, List<KeyValuePair<string, string>> aData)
        {
            var content = new FormUrlEncodedContent(aData);
            var response = fClient.PutAsync(SimpleIni.Ini.Value("GitLabApiPath") + "/" + aRelativeURL, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;
                string responseString = responseContent.ReadAsStringAsync().Result;
                return JToken.Parse(responseString);
            }
            else
            {
                Debug.WriteLine("## error in PUT request on " + aRelativeURL + ": " + response.StatusCode);
                return null;
            }
        }

        private JToken RequestPost(string aRelativeURL, JToken aData)
        {
            var content = new StringContent(aData.ToString(), Encoding.UTF8, "application/json");
            var response = fClient.PostAsync(SimpleIni.Ini.Value("GitLabApiPath") + "/" + aRelativeURL, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;
                string responseString = responseContent.ReadAsStringAsync().Result;
                return JToken.Parse(responseString);
            }
            else
            {
                Debug.WriteLine("## error in POST request on " + aRelativeURL + ": " + response.StatusCode);
                return null;
            }
        }

        private string RequestDelete(string aRelativeURL)
        {
            var response = fClient.DeleteAsync(SimpleIni.Ini.Value("GitLabApiPath") + "/" + aRelativeURL).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;
                return responseContent.ReadAsStringAsync().Result;
            }
            else
            {
                Debug.WriteLine("## error in DELETE request on " + aRelativeURL + ": " + response.StatusCode);
                return null;
            }
        }

        public Dictionary<string, GitLabProject> Projects { get { return fProjectsHTTP; } }

        public const string DataFileName = "data.json";
        private readonly HttpClient fClient;
        private Dictionary<string, GitLabProject> fProjectsHTTP;
        private readonly Dictionary<string, GitLabProject> fProjectsSSH = new();
    }

    public class GitLabProject
    {
        public GitLabProject() { }
        public GitLabProject(JToken aProjectJSON, bool aHasJobs = true)
        {
            if (aProjectJSON != null)
            {
                ID = aProjectJSON["id"].Value<int>();
                Name = aProjectJSON["name_with_namespace"].Value<string>();
                Path = aProjectJSON["path_with_namespace"].Value<string>();
                Created = aProjectJSON["created_at"].Value<DateTime>();
                LastActive = aProjectJSON["last_activity_at"].Value<DateTime>();
                WEBURL = aProjectJSON["web_url"].Value<string>();
                Description = aProjectJSON["description"].Value<string>();
                URLHttp = aProjectJSON["http_url_to_repo"].Value<string>();
                URLSSH = aProjectJSON["ssh_url_to_repo"].Value<string>();
                HasJobs = aHasJobs;
            }
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string Description { get; set; }
        public string WEBURL { get; set; }
        public string URLHttp { get; set; }
        public string URLSSH { get; set; }
        [DefaultValue(false)]
        public bool IsSubmodule { get; set; }
        public Dictionary<string, GitLabBranch> Branches { get; } = new Dictionary<string, GitLabBranch>();

        [DefaultValue(false)]
        public bool HasJobs { get; set; }
    }

    public class GitLabBranch
    {
        public GitLabBranch() { }
        public GitLabBranch(JToken aBranchJSON)
        {
            if (aBranchJSON != null)
            {
                Name = aBranchJSON["name"].Value<string>();
                Commit = aBranchJSON["commit"]["id"].Value<string>();
                IsProtected = aBranchJSON["protected"].Value<bool>();
                IsDefault = aBranchJSON["default"].Value<bool>();
            }
        }
        public string Name { get; set; }
        public string Commit { get; set; }
        public bool IsProtected { get; set; }
        public bool IsDefault { get; set; }
        public Dictionary<string, GitLabSubmodule> Submodules { get; set; } = new Dictionary<string, GitLabSubmodule>();
        public Dictionary<string, GitLabJob> Jobs { get; } = new Dictionary<string, GitLabJob>();
        
        [DefaultValue(0)]
        public int Source { get; set; }
        public bool Transform { get; set; }
        public bool Packages { get; set; }
        public string ProjectVersion { get; set; }
        public List<string> SolutionFiles { get; set; } = new List<string>();
        public List<string> ProjectFiles { get; set; } = new List<string>();

        //[JsonIgnore]
        //public List<GitLabSubmodule> IsSubmoduleFor { get; set; } = new List<GitLabSubmodule>();
    }

    public class GitLabSubmodule
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string URL { get; set; }
        public string Branch { get; set; }
        public string Commit { get; set; }
        public string HeadCommit { get; set; }

        [DefaultValue(false)]
        public bool IsOnHead { get; set; }
        public bool IsValid { get; set; }

    }

    public class GitLabJob
    {
        public GitLabJob() { }
        public GitLabJob(JToken aJob)
        {
            Name = aJob["name"].Value<string>();
            Stage = aJob["stage"].Value<string>();
            ID = aJob["id"].Value<int>();
            Commit = aJob["commit"]["id"].Value<string>();
            Status = aJob["status"].Value<string>();
        }
        public string Name { get; set; }
        public string Stage { get; set; }
        public int ID { get; set; }
        public string Commit { get; set; }
        public string Status { get; set; }
    }
}
