﻿using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GitLabHelperGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            BuildJobStateBrushes();
            fHelper = new Helper();

            fAutoBuildBehindProjects = CreateMainGroup("Auto builds behind", true);
            fAutoBuildUpToDateProjects = CreateMainGroup("Auto builds uptodate");
            fSubmodulesProjects = CreateMainGroup("Sub modules");
            fOtherBehindProjects = CreateMainGroup("Other behind");
            fOtherUpToDateProjects = CreateMainGroup("Other uptodate");

            SyncGitlabToUI(null, "Syncing tree");
            fTimer = new System.Threading.Timer((state) => { UpdateJobsAndKnownSubmodulesOnTimer(); }, null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            DataContext = this;

            TimerCheckBox.IsChecked = true;
        }

        private readonly Helper fHelper;
        private readonly System.Threading.Timer fTimer;
        public ObservableCollection<UIGroup> Groups { get; set; } = new ObservableCollection<UIGroup>();

        private readonly Dictionary<string, SolidColorBrush> fJobStateBrushes = new Dictionary<string, SolidColorBrush>();

        private readonly UIGroup fAutoBuildBehindProjects;
        private readonly UIGroup fAutoBuildUpToDateProjects;
        private readonly UIGroup fSubmodulesProjects;
        private readonly UIGroup fOtherBehindProjects;
        private readonly UIGroup fOtherUpToDateProjects;

        private const int jsiBehind = 22;
        private const int jsiHead = 21;

        private const int jsiRunning = 16;
        private const int jsiPending = 15;
        private const int jsiFailed = 14;
        private const int jsiCanceled = 13;
        private const int jsiSkipped = 12;
        private const int jsiSuccess = 11;

        private const int jsiManual = 6;
        private const int jsiCreated = 5;

        private const int jsiOther = 0;

        private const string jssBehind = "behind";
        private const string jssHead = "head";

        private const string jssRunning = "running";
        private const string jssPending = "pending";
        private const string jssFailed = "failed";
        private const string jssCanceled = "cancelled";
        private const string jssSkipped = "skipped";
        private const string jssSuccess = "success";

        private const string jssManual = "manual";
        private const string jssCreated = "created";

        private int JobStateToInt(string aJobState)
        {
            return aJobState switch
            {
                jssBehind => jsiBehind,
                jssHead => jsiHead,
                jssRunning => jsiRunning,
                jssPending => jsiPending,
                jssFailed => jsiFailed,
                jssCanceled => jsiCanceled,
                jssSkipped => jsiSkipped,
                jssSuccess => jsiSuccess,
                jssManual => jsiManual,
                jssCreated => jsiCreated,
                _ => jsiOther,
            };
        }

        private void BuildJobStateBrushes()
        {
            // job
            fJobStateBrushes.Add(jssSuccess, new SolidColorBrush(Colors.LimeGreen));
            fJobStateBrushes.Add(jssFailed, new SolidColorBrush(Colors.Red));
            fJobStateBrushes.Add(jssRunning, new SolidColorBrush(Colors.Blue));
            fJobStateBrushes.Add(jssPending, new SolidColorBrush(Colors.Yellow));
            fJobStateBrushes.Add(jssCanceled, new SolidColorBrush(Colors.White));
            fJobStateBrushes.Add(jssSkipped, new SolidColorBrush(Colors.Gray));
            fJobStateBrushes.Add(jssManual, new SolidColorBrush(Colors.LightGray));
            fJobStateBrushes.Add(jssCreated, new SolidColorBrush(Colors.Orange));
            // sub modules
            fJobStateBrushes.Add(jssHead, new SolidColorBrush(Colors.DodgerBlue));
            fJobStateBrushes.Add(jssBehind, new SolidColorBrush(Colors.Gold));
            // other
            fJobStateBrushes.Add("", new SolidColorBrush(Colors.LightGray));
        }

        private void SumJobState(ref string aCurrentState, string aNewState)
        {
            if (JobStateToInt(aNewState) > JobStateToInt(aCurrentState))
                aCurrentState = aNewState;
        }

        private UIGroup CreateMainGroup(string aName, bool aIsExpanded = false)
        {
            var uiGroup = new UIGroup() { Name = aName, IsExpanded = aIsExpanded };
            Groups.Add(uiGroup);
            return uiGroup;
        }

        public void UpdateJobsAndKnownSubmodulesOnTimer()
        {
            Debug.WriteLine(">> timer triggered update");
            lock (fHelper)
            {
                // update gitlab job states
                fHelper.UpdateAllJobs(null, "");
                Dispatcher.BeginInvoke(new Action(() => { SyncGitlabToUI(null, "Syncing tree"); }));
            }
            Debug.WriteLine(">> timer done updating");
        }

        private void SyncGitlabToUI(Action<int, int, string> aSetProgress, string aStage)
        {
            // sync ui with gitlab state (no removes for now..)
            lock (fHelper)
            {
                int i = 0;
                // clear all group states
                aSetProgress?.Invoke(i, fHelper.Projects.Count + 2, aStage);
                foreach (var uiGroup in Groups)
                {
                    // remove ui projects no longer in gitlab
                    var removeProjects = new List<UIProject>();
                    foreach (var uiProject in uiGroup.Projects)
                        if (!fHelper.Projects.ContainsKey(uiProject.Key))
                            removeProjects.Add(uiProject);
                    foreach (var uiProject in removeProjects)
                    {
                        uiProject.Clear();
                        uiGroup.Projects.Remove(uiProject);
                    }
                }
                aSetProgress?.Invoke(i++, fHelper.Projects.Count + 2, aStage);
                foreach (var project in fHelper.Projects)
                {
                    // find current ui group and ui project for gitlab project
                    UIGroup uiGroup = null;
                    UIGroup uiNewGroup = null;
                    UIProject uiProject = null;
                    foreach (var uiGroup2 in Groups)
                    {
                        foreach (var uiProject2 in uiGroup2.Projects)
                        {
                            if (uiProject2.ID == project.Value.ID)
                            {
                                uiGroup = uiGroup2;
                                uiProject = uiProject2;
                                break;
                            }
                        }
                        if (uiGroup != null)
                            break;
                    }
                    // create ui project if not found (do not add to group yet)
                    if (uiProject == null)
                        uiProject = new UIProject()
                        {
                            Name = project.Value.Name,
                            ID = project.Value.ID,
                            Key = project.Key,
                            URLHttp = project.Value.URLHttp,
                            URLSsh = project.Value.URLSSH,
                            WEBURL = project.Value.WEBURL
                        };
                    else
                    {
                        // remove ui branches no longer in gitlab
                        var removeBranches = new List<UIBranch>();
                        foreach (var uiBranch in uiProject.Branches)
                            if (!project.Value.Branches.ContainsKey(uiBranch.Name))
                                removeBranches.Add(uiBranch);
                        foreach (var uiBranch in removeBranches)
                        {
                            uiBranch.Clear();
                            uiProject.Branches.Remove(uiBranch);
                        }
                    }
                    // uiGroup might be null
                    // uiProject is defined but maybe needs adding to a group
                    // reset project counters and status
                    uiProject.smcOK = 0;
                    uiProject.smcBehind = 0;
                    uiProject.smcInvalid = 0;
                    uiProject.Status = "";
                    int projectSource = 0;
                    // parse all known gitlab branches
                    foreach (var gitlabBranch in project.Value.Branches)
                    {
                        // find ui branch with gitlab branch
                        UIBranch uiBranch = null;
                        foreach (var uiBranch2 in uiProject.Branches)
                        {
                            if (uiBranch2.Name == gitlabBranch.Key)
                            {
                                uiBranch = uiBranch2;
                                break;
                            }
                        }
                        // if ui branch not found create and add it to ui project
                        if (uiBranch == null)
                        {
                            uiBranch = new UIBranch() { Name = gitlabBranch.Key, Parent = uiProject };
                            uiProject.Branches.Add(uiBranch);
                        }
                        else
                        {
                            // remove ui submodules no longer in gitlab
                            var removeSubmodules = new List<UISubmodule>();
                            foreach (var uiSubmodule in uiBranch.Submodules)
                                if (!gitlabBranch.Value.Submodules.ContainsKey(uiSubmodule.Name))
                                    removeSubmodules.Add(uiSubmodule);
                            foreach (var uiSubmodule in removeSubmodules)
                            {
                                uiSubmodule.Clear();
                                uiBranch.Submodules.Remove(uiSubmodule);
                            }
                            // remove ui jobs no longer in gitlab
                            var removeJobs = new List<UIJob>();
                            foreach (var uiJob in uiBranch.Jobs)
                                if (!gitlabBranch.Value.Jobs.ContainsKey(uiJob.Name))
                                    removeJobs.Add(uiJob);
                            foreach (var uiJob in removeJobs)
                            {
                                uiJob.Clear();
                                uiBranch.Jobs.Remove(uiJob);
                            }
                        }
                        // reset branch counters and status
                        uiBranch.smcOK = 0;
                        uiBranch.smcBehind = 0;
                        uiBranch.smcInvalid = 0;
                        uiBranch.Status = "";
                        // parse all known gitlab sub modules on branch
                        foreach (var gitlabSubmodule in gitlabBranch.Value.Submodules)
                        {
                            // find ui submodule with gitlab submodule
                            UISubmodule uiSubmodule = null;
                            foreach (var uiSubmodule2 in uiBranch.Submodules)
                            {
                                if (uiSubmodule2.Path == gitlabSubmodule.Value.Path)
                                {
                                    uiSubmodule = uiSubmodule2;
                                    break;
                                }
                            }
                            // if ui submodule not found add create and add it to ui branch else update it
                            if (uiSubmodule == null)
                            {
                                uiSubmodule = new UISubmodule() { Name = gitlabSubmodule.Key, IsOnHead = gitlabSubmodule.Value.IsOnHead, HeadCommit = gitlabSubmodule.Value.HeadCommit, Path = gitlabSubmodule.Value.Path, Parent = uiBranch };
                                uiBranch.Submodules.Add(uiSubmodule);
                            }
                            else
                            {
                                // update subodule commit status
                                uiSubmodule.IsOnHead = gitlabSubmodule.Value.IsOnHead;
                                uiSubmodule.HeadCommit = gitlabSubmodule.Value.HeadCommit;
                            }
                            if (gitlabSubmodule.Value.IsOnHead)
                            {
                                uiProject.smcOK++;
                                uiBranch.smcOK++;
                                uiSubmodule.StateColor = fJobStateBrushes[jssHead];
                            }
                            else
                            {
                                uiProject.smcBehind++;
                                uiBranch.smcBehind++;
                                uiSubmodule.StateColor = fJobStateBrushes[jssBehind];
                            }
                            if (!gitlabSubmodule.Value.IsValid)
                            {
                                uiProject.smcInvalid++;
                                uiBranch.smcInvalid++;
                                uiSubmodule.StateColor = fJobStateBrushes[jssFailed];
                            }
                        }
                        foreach (var gitlabJob in gitlabBranch.Value.Jobs)
                        {
                            // find ui submodule with gitlab submodule
                            UIJob uiJob = null;
                            foreach (var uiJob2 in uiBranch.Jobs)
                            {
                                if (uiJob2.Name == gitlabJob.Key)
                                {
                                    uiJob = uiJob2;
                                    break;
                                }
                            }
                            // if ui submodule not found add create and add it to ui branch else update it
                            if (uiJob == null)
                            {
                                uiJob = new UIJob() { Name = gitlabJob.Key, Parent = uiBranch, Status = gitlabJob.Value.Status };
                                uiBranch.Jobs.Add(uiJob);
                            }
                            else
                            {
                                // update job status
                                uiJob.Status = gitlabJob.Value.Status;
                            }
                            uiJob.StateColor = fJobStateBrushes[uiJob.Status];
                        }
                        uiBranch.SubmodulesAndJobsUpdated();
                        uiBranch.SubmoduleOKCount = uiBranch.smcOK > 0 ? uiBranch.smcOK.ToString() : "";
                        uiBranch.SubmoduleBehindCount = uiBranch.smcBehind > 0 ? uiBranch.smcBehind.ToString() : "";
                        uiBranch.SubmoduleInvalidCount = uiBranch.smcInvalid > 0 ? uiBranch.smcInvalid.ToString() : "";
                        foreach (var job in gitlabBranch.Value.Jobs)
                        {
                            SumJobState(ref uiBranch.Status, job.Value.Status);
                            SumJobState(ref uiProject.Status, job.Value.Status);
                        }
                        uiBranch.StateColor = fJobStateBrushes[uiBranch.Status];
                        uiBranch.Source = gitlabBranch.Value.Source;
                        uiBranch.Transform = gitlabBranch.Value.Transform;
                        uiBranch.Packages = gitlabBranch.Value.Packages;
                        uiBranch.ProjectVersion = gitlabBranch.Value.ProjectVersion;
                        uiBranch.Solutions.Clear();
                        foreach (var solutionFile in gitlabBranch.Value.SolutionFiles)
                            uiBranch.Solutions.Add(solutionFile);
                        // todo: **** uiBranch.Image = "/GitLabHelperGUI;component/Images/" + gitlabBranch.Value.Source.ToString() + ".png";
                        projectSource |= gitlabBranch.Value.Source;
                    }
                    // project entry
                    uiProject.SubmoduleBehindCount = uiProject.smcBehind > 0 ? uiProject.smcBehind.ToString() : "";
                    uiProject.SubmoduleOKCount = uiProject.smcOK > 0 ? uiProject.smcOK.ToString() : "";
                    uiProject.SubmoduleInvalidCount = uiProject.smcInvalid > 0 ? uiProject.smcInvalid.ToString() : "";
                    uiProject.StateColor = fJobStateBrushes[uiProject.Status];
                    // todo: **** uiProject.Image = "/GitLabHelperGUI;component/Images/" + projectSource.ToString() + ".png";
                    // find a group to add project to that matches the project status 
                    if (project.Value.HasJobs)
                        uiNewGroup = uiProject.smcBehind > 0 ? fAutoBuildBehindProjects : fAutoBuildUpToDateProjects;
                    else if (project.Value.IsSubmodule)
                        uiNewGroup = fSubmodulesProjects;
                    else
                        uiNewGroup = uiProject.smcBehind > 0 ? fOtherBehindProjects : fOtherUpToDateProjects;
                    // check if we have to move to new group
                    if (uiGroup == null)
                    {
                        uiNewGroup.Projects.Add(uiProject);
                        uiProject.Parent = uiNewGroup;
                    }
                    else
                    {
                        if (uiGroup != uiNewGroup)
                        {
                            uiNewGroup.Projects.Add(uiProject);
                            uiProject.Parent = uiNewGroup;
                            uiGroup.Projects.Remove(uiProject);
                        }
                    }
                    aSetProgress?.Invoke(i++, fHelper.Projects.Count + 2, aStage);
                }
                foreach (var uiGroup in Groups)
                {
                    uiGroup.Status = "";
                    uiGroup.smcOK = 0;
                    uiGroup.smcBehind = 0;
                    uiGroup.smcInvalid = 0;
                    foreach (var uiProject in uiGroup.Projects)
                    {
                        uiGroup.smcOK += uiProject.smcOK;
                        uiGroup.smcBehind += uiProject.smcBehind;
                        uiGroup.smcInvalid += uiProject.smcInvalid;
                        SumJobState(ref uiGroup.Status, uiProject.Status);
                    }
                    uiGroup.SubmoduleBehindCount = uiGroup.smcBehind > 0 ? uiGroup.smcBehind.ToString() : "";
                    uiGroup.SubmoduleOKCount = uiGroup.smcOK > 0 ? uiGroup.smcOK.ToString() : "";
                    uiGroup.SubmoduleInvalidCount = uiGroup.smcInvalid > 0 ? uiGroup.smcInvalid.ToString() : "";
                    uiGroup.StateColor = fJobStateBrushes[uiGroup.Status];
                    // sort projects in group
                    var projectList = uiGroup.Projects.ToList();
                    projectList.Sort((a, b) => a.Name.CompareTo(b.Name));
                    uiGroup.Projects.Clear();
                    foreach (var project in projectList)
                        uiGroup.Projects.Add(project);
                }
                aSetProgress?.Invoke(i++, fHelper.Projects.Count + 2, aStage);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void RescanAll_Button_Click(object sender, RoutedEventArgs e)
        {
            StartHelperJob(fHelper.Rescan, true);
        }

        private void RescanAllJobs_Button_Click(object sender, RoutedEventArgs e)
        {
            StartHelperJob(fHelper.UpdateAllJobs, true);
        }

        private void RescanAllSubmodules_Button_Click(object sender, RoutedEventArgs e)
        {
            StartHelperJob(fHelper.UpdateAllSubmodules, true);
        }

        private void PutSubmoduleOnHead(UISubmodule aSubmodule)
        {
            if (fHelper.PutSubmoduleOnCommitInGitlab(aSubmodule.Parent.Parent.ID, aSubmodule.Parent.Name, aSubmodule.Path, aSubmodule.HeadCommit))
            {
                // find GitLabSubmodule and put Commit = HeadCommit
                if (!fHelper.SetSubmoduleCommitOnHead(aSubmodule.Parent.Parent.ID, aSubmodule.Parent.Name, aSubmodule.Name))
                    Debug.WriteLine("## could not find submodule to reset commit to head");
            }
        }

        private string[] AdjustSubmodulesPaths(UIBranch aBranch, bool aMakeRelative, out bool aChanged)
        {
            aChanged = false;
            var linesGitModules = fHelper.RequestProjectFile(aBranch.Parent.ID, aBranch.Name, ".gitmodules").Split("\n");
            if (linesGitModules != null)
            {
                for (var i = 0; i < linesGitModules.Length; i++)
                {
                    var line = linesGitModules[i];
                    var split = line.Split("=");
                    if (split.Length == 2)
                    {
                        var item = split[0].Trim();
                        if (item.Equals("url", StringComparison.OrdinalIgnoreCase))
                        {
                            var currentURL = split[1].Trim();
                            if (currentURL.ToLower().StartsWith("git@"))
                            {
                                // todo: test make absolute, needs tweaking to rebuild the git uri from the ssh uri (ssh:// prefix removed, first / replaced by : again?
                                // todo: make absolute always goes back to http becase git@ is removed!
                                var newURL = aMakeRelative ?
                                    Helper.MakeRelativeURIPath("ssh://" + aBranch.Parent.URLSsh.Replace(":", "/"), "ssh://" + currentURL.Replace(":", "/")) :
                                    Helper.MakeAbsoluteURIPath("ssh://" + aBranch.Parent.URLSsh.Replace(":", "/"), "ssh://" + currentURL.Replace(":", "/"));
                                if (!currentURL.Equals(newURL, StringComparison.OrdinalIgnoreCase))
                                {
                                    linesGitModules[i] = line.Replace(currentURL, newURL);
                                    aChanged = true;
                                }
                            }
                            else
                            {
                                var newURL = aMakeRelative ? Helper.MakeRelativeURIPath(aBranch.Parent.URLHttp, currentURL) : Helper.MakeAbsoluteURIPath(aBranch.Parent.URLHttp, currentURL);
                                if (!currentURL.Equals(newURL, StringComparison.OrdinalIgnoreCase))
                                {
                                    linesGitModules[i] = line.Replace(currentURL, newURL);
                                    aChanged = true;
                                }
                            }
                        }
                    }
                }
            }
            return linesGitModules;
        }

        private void UpdateSubmodulesPaths(UIBranch aBranch, bool aMakeRelative)
        {
            var linesGitModules = AdjustSubmodulesPaths(aBranch, aMakeRelative, out bool changedGitModules);
            if (changedGitModules)
            {
                string actionGitModules = "update"; // .submodules always exists or we signal no change
                JArray jActions = new JArray
                {
                    // add action
                    new JObject(
                        new JProperty("action", actionGitModules),
                        new JProperty("file_path", ".gitmodules"),
                        new JProperty("content", String.Join("\n", linesGitModules)))
                };
                // commit actions
                JObject jCommit =
                    new JObject(
                        new JProperty("branch", aBranch.Name),
                        new JProperty("commit_message", "make submodules paths " + (aMakeRelative ? "relative" : "absolute")),
                        new JProperty("actions", jActions));
                var jResult = fHelper.PostCommit(aBranch.Parent.ID, jCommit);
                Debug.WriteLine(">> Commit result: " + jResult?.ToString() ?? "## NO RESULT");
            }
        }

        private void ResetBuildJob(UIBranch aBranch)
        {
            // .gitmodules: make relative
            string actionGitModules = "update"; // .submodules always exists or we signal no change
            var linesGitModules = AdjustSubmodulesPaths(aBranch, true, out bool changedGitModules);
            // yaml: build script
            var contentGitLabCIYML = fHelper.RequestProjectFile(aBranch.Parent.ID, aBranch.Name, ".gitlab-ci.yml");
            string platform = "";
            string solution = System.IO.Path.GetFileNameWithoutExtension(aBranch.Solutions.First());

            List<string> scriptLines = new List<string>() {
                "# auto generated script",
                "",
                "variables:",
                "# support submodules",
                "  GIT_SUBMODULE_STRATEGY: recursive",
                "",
                "before_script:",
                "  - git submodule sync --recursive",
                "  - git submodule update --init --recursive",
                "",
                "# jobs",
                "stages:",
                "  - build",
                //"#  - test",
                "  - deploy",
            };

            if (BitsAllSet(aBranch.Source, Helper.srcDotNetCore))
            {
                platform = "portable";
                scriptLines.AddRange(new List<string>() {
                    "",
                    "# build binary from source, create output folders, compile and link",
                    "build:",
                    "  stage: build",
                    "  tags:",
                    "    - windows",
                    "  script:",
                    "    - $msbuild = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find MSBuild\\**\\Bin\\MSBuild.exe",
                    "    - $nuget = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find.\\**\\Tools\\nuget.exe",
                    "    - if (" + (aBranch.Packages ? "$true" : "Test-path -path \"packages.config\"") + ") { & $nuget restore }",
                    "    - if (" + (aBranch.Transform ? "$true" : "$false") + ") { & $msbuild %sln%.sln -t:TransformAll }",
                    "    - if ($true) { & $msbuild %sln%.sln -t:restore }",
                    "    - if ($true) { & $msbuild %sln%.sln -p:Configuration=Release }",
                    //"    - if ($true) { & $msbuild %sln%.sln -p:DeployOnBuild=true -p:PublishProfile=portable }",
                    //"    - if ($true) { & $msbuild %sln%.sln /p:Configuration=Release /p:\"DeployOnBuild=true;PublishProfile=portable\" }",
                    "    - if ($true) { & dotnet publish /p:PublishProfile=portable }",
                    "  artifacts:",
                    "    paths:",
                    "      - bin\\publish-portable\\*.*"
                });
            }
            else if (BitsAllSet(aBranch.Source, Helper.srcWindows | Helper.srcCpp) || BitsAllSet(aBranch.Source, Helper.srcWindows | Helper.srcCsharp))
            {
                platform = "windows";
                scriptLines.AddRange(new List<string>() {
                    "",
                    "# build binary from source, create output folders, compile and link",
                    "build:",
                    "  stage: build",
                    "  tags:",
                    "    - windows",
                    "  script:",
                    "    - $msbuild = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find MSBuild\\**\\Bin\\MSBuild.exe",
                    "    - $nuget = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find.\\**\\Tools\\nuget.exe",
                    "    - if (" + (aBranch.Packages ? "$true" : "Test-path -path \"packages.config\"") + ") { & $nuget restore }",
                    "    - if ($true) { & $msbuild %sln%.sln -t:restore }",
                    "    - if ($true) { & $msbuild %sln%.sln -p:Configuration=Release }",
                    "  artifacts:",
                    "    paths:"
                });
                if (BitsAllSet(aBranch.Source, Helper.srcCpp))
                {
                    scriptLines.AddRange(new List<string>() {
                        "      - x64\\Release\\*.exe"
                    });
                }
                if (BitsAllSet(aBranch.Source, Helper.srcCsharp))
                {
                    scriptLines.AddRange(new List<string>() {
                        "      - bin\\Release\\*.exe",
                        "      - bin\\Release\\*.dll",
                        "      - bin\\Release\\*.config",
                        "      - bin\\Release\\*.xml"
                    });
                }
            }
            else if (BitsAllSet(aBranch.Source, Helper.srcWindows | Helper.srcDelphi))
            {
                platform = "windows";
                // convert ProjectVersion to Delphi version in folder name
                //18.4 -> "19.0" Delphi 10.2 update 3
                //18.7 -> "20.0" Delphi 10.3
                int delphiProjectVersionx10 = int.TryParse(aBranch.ProjectVersion.Replace(".", ""), out int version) ? version : 0;
                string delphiFolderVersion = delphiProjectVersionx10 <= 184 ? "19.0" : "20.0";
                scriptLines.AddRange(new List<string>() {
                    "",
                    "# build binary from source, create output folders, compile and link",
                    "build:",
                    "  stage: build",
                    "  tags:",
                    "    - windows",
                    "  script:",
                    "    - $msbuild = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find MSBuild\\**\\Bin\\MSBuild.exe",
                    "    - $nuget = & \"C:\\Program Files (x86)\\Microsoft Visual Studio\\Installer\\vswhere.exe\" -latest -products * -find.\\**\\Tools\\nuget.exe",
                    //"    - start \"C:\\Program Files (x86)\\Embarcadero\\Studio\\" + delphiFolderVersion + "\\bin\\rsvars.bat\" -Wait -NoNewWindow",
                    "    - $ENV:BDS = \"C:\\Program Files (x86)\\Embarcadero\\Studio\\" + delphiFolderVersion + "\"",
                    "    - $ENV:BDSINCLUDE = \"C:\\Program Files (x86)\\Embarcadero\\Studio\\" + delphiFolderVersion + "\\include\"",
                    "    - $ENV:BDSCOMMONDIR = \"C:\\Users\\Public\\Documents\\Embarcadero\\Studio\\" + delphiFolderVersion + "\"",
                    "    - if (" + (aBranch.Packages ? "$true" : "Test-path -path \"packages.config\"") + ") { & $nuget restore }",
                    "    - if ($true) { & $msbuild %sln%.dproj -p:config=Release -p:platform=Win64 }",
                    "  artifacts:",
                    "    paths:"
                });
                scriptLines.AddRange(new List<string>() {
                    "      - Win64\\Release\\*.exe"
                });
            }

            /*
            scriptLines.AddRange(new List<string>() {
                "",
                "# run tests using the binary built before",
                "test:",
                "  stage: test",
                "  tags:",
                "    - windows",
                "  script:",
                "    - start scripts\\test.bat -Wait -NoNewWindow"
            });
            */

            scriptLines.AddRange(new List<string>() {
                "",
                "# upload binary to own repository, trigger manually only",
                "deploy:",
                "  stage: deploy",
                "  tags:",
                "    - windows",
                "  script:",
                "    - start scripts\\deploy.bat -Wait -NoNewWindow -ArgumentList \"%sln%-%platform%\"",
                "  when: manual"
            });

            string script = platform.Length > 0 ? String.Join("\n", scriptLines).Replace("%sln%", solution).Replace("%platform%", platform) : "";
            bool changedScript = platform.Length > 0 && (!contentGitLabCIYML?.Equals(script) ?? true);
            string actionScript = contentGitLabCIYML != null ? "update" : "create";

            // commit
            // we have a change: build commit
            JArray jActions = new JArray();
            if (changedGitModules)
            {
                jActions.Add(
                    new JObject(
                        new JProperty("action", actionGitModules),
                        new JProperty("file_path", ".gitmodules"),
                        new JProperty("content", String.Join("\n", linesGitModules))));
            }
            if (changedScript)
            {
                jActions.Add(
                    new JObject(
                        new JProperty("action", actionScript),
                        new JProperty("file_path", ".gitlab-ci.yml"),
                        new JProperty("content", script)));
            }

            // cleanup scripts
            if (platform.Length > 0)
            {
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/.gitlab-ci.yml.template");
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/blbs.exe");
                ActionFileRemoveIfExists(jActions, aBranch, "blbs.exe");
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/blbs.log");
                ActionFileRemoveIfExists(jActions, aBranch, "blbs.log");
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/findvs.bat");
                ActionFileRemoveIfExists(jActions, aBranch, "findvs.bat");
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/nuget.exe");
                ActionFileRemoveIfExists(jActions, aBranch, "nuget.exe");
                ActionFileRemoveIfExists(jActions, aBranch, "scripts/vswhere.exe");
                ActionFileRemoveIfExists(jActions, aBranch, "vswhere.exe");
            }

            //
            //File.WriteAllText(".gitlab-ci.yml", script);
            //File.WriteAllText(".gitmodules", String.Join("\n", linesGitModules));

            if (jActions.Count > 0)
            {
                JObject jCommit =
                    new JObject(
                        new JProperty("branch", aBranch.Name),
                        new JProperty("commit_message", "update of ci job"),
                        new JProperty("actions", jActions));
                File.WriteAllText("commit.json", jCommit.ToString());
                var jResult = fHelper.PostCommit(aBranch.Parent.ID, jCommit);
                Debug.WriteLine(">> Commit result: " + jResult?.ToString() ?? "## NO RESULT");
                if (jResult != null)
                    File.WriteAllText("commit-result.json", jResult.ToString());
            }
            else
                Debug.WriteLine(">> NO actions for commit");
        }

        private void RemoveOldPipelines(UIProject aProject, string aBranch)
        {
            var jPipelines = fHelper.RequestPipelines(aProject.ID, aBranch);
            if (jPipelines is JArray jArray)
            {
                var pipelines = new Dictionary<string, List<int>>();
                foreach (var jPipeline in jArray)
                {
                    var id = jPipeline["id"].Value<int>();
                    var branch = jPipeline["ref"].Value<string>();
                    if (pipelines.TryGetValue(branch, out var idList))
                        idList.Add(id);
                    else
                        pipelines.Add(branch, new List<int>() { id });
                }
                if (aBranch.Length == 0)
                {
                    // remove first pipeline if pipeline branch is in project branches
                    foreach (var pipelineBranch in pipelines)
                    {
                        foreach (var projectBranch in aProject.Branches)
                        {
                            if (projectBranch.Name.Equals(pipelineBranch.Key, StringComparison.OrdinalIgnoreCase))
                            {
                                pipelineBranch.Value.RemoveAt(0);
                                break;
                            }
                        }
                    }
                    // delete all that are left
                    Parallel.ForEach(pipelines, (list) =>
                    {
                        Parallel.ForEach(list.Value, (pipelineID) => fHelper.RequestPipelineDelete(aProject.ID, pipelineID));
                    });
                }
                else
                {
                    // we should have 1 entry in pipelines that matches aBranch
                    if (pipelines.Count == 1 && pipelines.TryGetValue(aBranch, out var list))
                    {
                        // delete all except first id in all lists
                        list.RemoveAt(0);
                        Parallel.ForEach(list, (pipelineID) => fHelper.RequestPipelineDelete(aProject.ID, pipelineID));
                    }
                    else
                        Debug.WriteLine(">> no pipelines or something is wrong..");
                }
            }
        }

        private void RemoveAllPipelines(UIProject aProject, string aBranch)
        {
            var jPipelines = fHelper.RequestPipelines(aProject.ID, aBranch);
            if (jPipelines is JArray jArray)
            {
                var pipelines = new Dictionary<string, List<int>>();
                foreach (var jPipeline in jArray)
                {
                    var id = jPipeline["id"].Value<int>();
                    var branch = jPipeline["ref"].Value<string>();
                    if (pipelines.TryGetValue(branch, out var idList))
                        idList.Add(id);
                    else
                        pipelines.Add(branch, new List<int>() { id });
                }
                if (aBranch.Length == 0)
                {
                    // remove first pipeline if pipeline branch is in project branches
                    foreach (var pipelineBranch in pipelines)
                    {
                        foreach (var projectBranch in aProject.Branches)
                        {
                            if (projectBranch.Name.Equals(pipelineBranch.Key, StringComparison.OrdinalIgnoreCase))
                            {
                                pipelineBranch.Value.RemoveAt(0);
                                break;
                            }
                        }
                    }
                    // delete all that are left
                    Parallel.ForEach(pipelines, (list) =>
                    {
                        Parallel.ForEach(list.Value, (pipelineID) => fHelper.RequestPipelineDelete(aProject.ID, pipelineID));
                    });
                }
                else
                {
                    // we should have 1 entry in pipelines that matches aBranch
                    if (pipelines.Count == 1 && pipelines.TryGetValue(aBranch, out var list))
                    {
                        // delete all except first id in all lists
                        list.RemoveAt(0);
                        Parallel.ForEach(list, (pipelineID) => fHelper.RequestPipelineDelete(aProject.ID, pipelineID));
                    }
                    else
                        Debug.WriteLine(">> no pipelines or something is wrong..");
                }
            }
        }

        private bool ActionFileRemoveIfExists(JArray aActions, UIBranch aBranch, string aFilePath)
        {
            if (fHelper.RequestProjectFileExists(aBranch.Parent.ID, aBranch.Name, aFilePath))
            {
                aActions.Add(
                    new JObject(
                        new JProperty("action", "delete"),
                        new JProperty("file_path", aFilePath)));
                Debug.WriteLine(">> add file remove action for " + aBranch.DebugID + "$" + aFilePath);
                return true;
            }
            else
                return false;
        }

        private bool BitsAllSet(int aValue, int aBits) { return (aValue & aBits) == aBits; }
        //private bool BitsAnySet(int aValue, int aBits) { return (aValue & aBits) != 0; }

        private void ForEachBranch(UIBase aNode, Action<UIBranch> aAction)
        {
            if (aNode is UIGroup group)
            {
                foreach (var project in group.Projects)
                    ForEachBranch(project, aAction);
            }
            else if (aNode is UIProject project)
            {
                foreach (var branch in project.Branches)
                    ForEachBranch(branch, aAction);
            }
            else if (aNode is UIBranch branch)
                aAction(branch);
        }

        private void DisableTimer()
        {
            fTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            Debug.WriteLine("disabled timer");
        }
        private void EnableTimer()
        {
            if (TimerCheckBox.IsChecked ?? false)
            {
                fTimer.Change(15 * 1000, 15 * 1000);
                Debug.WriteLine("enabled timer");
            }
            else
                Debug.WriteLine("skipped enabling timer");
        }

        private void StartHelperJob(Action<Action<int, int, string>> aAction, bool aShowProgressBar)
        {
            DisableTimer();
            EnableBlur(aShowProgressBar);
            Task.Run(() =>
            {
                lock (fHelper)
                    aAction(SetProgress);
                ScheduleUIReloadAndResetEffect();
            });
        }

        private void ScheduleUIReloadAndResetEffect()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                SyncGitlabToUI(SetProgress, "Syncing tree");
                DisableBlur();
                EnableTimer();
            }));
        }

        private void SetProgress(int aValue, int aCount, string aStage)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                ProgressStage.Text = aStage;
                Progress.Maximum = aCount;
                Progress.Value = aValue;
            }));
        }

        private void EnableBlur(bool aShowProgressBar)
        {
            Tree.Effect = new BlurEffect();
            if (aShowProgressBar)
            {
                ProgressStage.Text = "";
                Progress.Value = 0;
                ProgressPanel.Visibility = Visibility.Visible;
            }
        }

        private void DisableBlur()
        {
            Tree.Effect = null;
            ProgressPanel.Visibility = Visibility.Collapsed;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            fHelper.SaveData(SetProgress, "Saving");
        }

        private void MenuItem_CloneToClipBoard_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIProject project)
                Clipboard.SetText(project.URLHttp);
        }

        private void MenuItem_PutOnHead_Click(object sender, RoutedEventArgs e)
        {
            DisableTimer();
            int i = 0;
            // put submodules under selected on head
            if (Tree.SelectedItem != null)
            {
                if (Tree.SelectedItem is UIGroup group)
                {
                    /* too dangerous
                    if (MessageBox.Show(
                        "Put all submodules under the selected project group on head?", 
                        "Put submodules on head", 
                        MessageBoxButton.OKCancel, 
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        foreach (var project in group.Projects)
                        {
                            SetProgress(i++, group.Projects.Count, "Putting on head");
                            foreach (var branch in project.Branches)
                            {
                                foreach (var submodule in branch.Submodules)
                                {
                                    if (!submodule.IsOnHead)
                                        PutSubmoduleOnHead(submodule);
                                }
                            }
                        }
                        SetProgress(i++, group.Projects.Count, "Putting on head");
                        SyncGitlabToUI(SetProgress, "Syncing tree");
                    }
                    */
                }
                else if (Tree.SelectedItem is UIProject project)
                {
                    if (MessageBox.Show(
                        "Put " + (project.SubmoduleBehindCount?.ToString() ?? "NO") + " submodules under the selected project on head?",
                        "Put submodules on head",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        foreach (var branch in project.Branches)
                        {
                            SetProgress(i++, project.Branches.Count, "Putting on head");
                            foreach (var submodule in branch.Submodules)
                                if (!submodule.IsOnHead)
                                    PutSubmoduleOnHead(submodule);
                        }
                        SetProgress(i++, project.Branches.Count, "Putting on head");
                        SyncGitlabToUI(SetProgress, "Syncing tree");
                    }
                }
                else if (Tree.SelectedItem is UIBranch branch)
                {
                    if (MessageBox.Show(
                        "Put " + (branch.SubmoduleBehindCount?.ToString() ?? "NO") + " submodules under the selected branch on head?",
                        "Put submodules on head",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        foreach (var submodule in branch.Submodules)
                        {
                            SetProgress(i++, branch.Submodules.Count, "Putting on head");
                            if (!submodule.IsOnHead)
                                PutSubmoduleOnHead(submodule);
                        }
                        SetProgress(i++, branch.Submodules.Count, "Putting on head");
                        SyncGitlabToUI(SetProgress, "Syncing tree");
                    }
                }
                else if (Tree.SelectedItem is UISubmodule submodule)
                {
                    if (MessageBox.Show(
                        "Put the selected submodule on head?",
                        "Put submodules on head",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        SetProgress(i++, 1, "Putting on head");
                        PutSubmoduleOnHead(submodule);
                        SetProgress(i++, 1, "Putting on head");
                        SyncGitlabToUI(SetProgress, "Syncing tree");
                    }
                }
            }
            EnableTimer();
        }

        private void MenuItem_OpenWebPage_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIProject project)
                Process.Start(new ProcessStartInfo { FileName = project.WEBURL, UseShellExecute = true });
        }

        private void MenuItem_ResetBuildJob_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                if (selected is UIBranch branch)
                {
                    if (MessageBox.Show(
                        "Reset build job for branch " + branch.DebugID,
                        "Reset build job",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        ResetBuildJob(branch);
                    }
                }
                else if (selected is UIProject project)
                {
                    if (MessageBox.Show(
                        "Reset build job for all branches of project " + project.DebugID,
                        "Reset build job",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        ForEachBranch(selected, ResetBuildJob);
                    }
                }
            }
        }

        private void MenuItem_RescanProject_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                string projectURL = selected is UIProject project ? project.URLHttp : (selected is UIBranch branch ? branch.Parent.URLHttp : "");
                if (projectURL.Length > 0)
                {
                    DisableTimer();
                    EnableBlur(true);
                    Task.Run(() =>
                    {
                        lock (fHelper)
                            fHelper.RescanProject(SetProgress, projectURL);
                        ScheduleUIReloadAndResetEffect();
                    });
                }
            }
        }

        private void MenuItem_RemoveOldPipelines_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                if (selected is UIBranch branch)
                {
                    if (MessageBox.Show(
                        "Remove all old pipelines for branch " + branch.DebugID,
                        "Remove all old pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        RemoveOldPipelines(branch.Parent, branch.Name);
                    }
                }
                else if (selected is UIProject project)
                {
                    if (MessageBox.Show(
                        "Remove all old pipelines for all branches of project " + project.DebugID,
                        "Remove all old pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        RemoveOldPipelines(project, "");
                    }
                }
                else if (selected is UIGroup group)
                {
                    if (MessageBox.Show(
                        "Remove all old pipelines for all projects and branches within the selected group",
                        "Remove all old pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        foreach (var projectInGroup in group.Projects)
                            RemoveOldPipelines(projectInGroup, "");
                    }
                }
            }
        }

        private void MenuItem_RemoveAllPipelines_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                if (selected is UIBranch branch)
                {
                    if (MessageBox.Show(
                        "Remove ALL pipelines for branch " + branch.DebugID,
                        "Remove ALL pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        RemoveAllPipelines(branch.Parent, branch.Name);
                    }
                }
                else if (selected is UIProject project)
                {
                    if (MessageBox.Show(
                        "Remove ALL pipelines for all branches of project " + project.DebugID,
                        "Remove ALL pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        RemoveAllPipelines(project, "");
                    }
                }
                else if (selected is UIGroup group)
                {
                    if (MessageBox.Show(
                        "Remove ALL pipelines for all projects and branches within the selected group",
                        "Remove ALL pipelines",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        foreach (var projectInGroup in group.Projects)
                            RemoveAllPipelines(projectInGroup, "");
                    }
                }
            }
        }

        private void Timer_CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox checkbox)
            {
                if (checkbox.IsChecked ?? false)
                    EnableTimer();
                else
                    DisableTimer();
            }
        }

        private void MenuItem_ExpandAll_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                selected.ExpandAll();
            }
        }

        private void MenuItem_CollapseAll_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                selected.CollapseAll();
            }
        }

        private void MenuItem_SubmodulesRelative_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                if (selected is UIBranch branch)
                {
                    UpdateSubmodulesPaths(branch, true);
                }
                else if (selected is UIProject project)
                {
                    foreach (var branch2 in project.Branches)
                        UpdateSubmodulesPaths(branch2, true);
                }
            }
        }

        private void MenuItem_SubmodulesAbsolute_Click(object sender, RoutedEventArgs e)
        {
            if (Tree.SelectedValue is UIBase selected)
            {
                if (selected is UIBranch branch)
                {
                    UpdateSubmodulesPaths(branch, false);
                }
                else if (selected is UIProject project)
                {
                    foreach (var branch2 in project.Branches)
                        UpdateSubmodulesPaths(branch2, false);
                }
            }
        }
    }

    public class UIGroup : UIBase
    {
        public ObservableCollection<UIProject> Projects { get; set; } = new ObservableCollection<UIProject>();
        public override void Clear()
        {
            foreach (var project in Projects)
                project.Clear();
            Projects.Clear();
        }
        public override List<UIBase> Children()
        {
            var result = new List<UIBase>();
            result.AddRange(Projects.ToList());
            return result;
        }
    }

    public class UIProject : UIBase
    {
        public int ID { get; set; }
        public string Key { get; set; }
        // todo: **** public string Image { get; set; }
        public string URLHttp { get; set; }
        public string URLSsh { get; set; }
        public string WEBURL { get; set; }

        public UIGroup Parent { get; set; }
        public ObservableCollection<UIBranch> Branches { get; set; } = new ObservableCollection<UIBranch>();
        public override void Clear()
        {
            Parent = null;
            foreach (var branch in Branches)
                branch.Clear();
            Branches.Clear();
        }
        public override List<UIBase> Children()
        {
            var result = new List<UIBase>();
            result.AddRange(Branches.ToList());
            return result;
        }

        public string DebugID { get { return WEBURL; } }
    }

    public class UIBranch : UIBase
    {
        public UIProject Parent { get; set; }
        public int Source { get; set; }
        public bool Transform { get; set; }
        public bool Packages { get; set; }
        public string ProjectVersion { get; set; }
        public ObservableCollection<string> Solutions { get; set; } = new ObservableCollection<string>();
        // todo: **** public string Image { get; set; }
        public ObservableCollection<UISubmodule> Submodules { get; set; } = new ObservableCollection<UISubmodule>();
        public ObservableCollection<UIJob> Jobs { get; set; } = new ObservableCollection<UIJob>();
        public List<UIBase> SubmodulesAndJobs
        {
            get
            {
                var result = new List<UIBase>();
                result.AddRange(Submodules.ToList());
                result.AddRange(Jobs.ToList());
                return result;
            }
        }
        public void SubmodulesAndJobsUpdated() { NotifyPropertyChanged("SubmodulesAndJobs"); }
        public override void Clear()
        {
            Parent = null;
            foreach (var submodule in Submodules)
                submodule.Clear();
            Submodules.Clear();
            foreach (var job in Jobs)
                job.Clear();
            Jobs.Clear();
        }
        public override List<UIBase> Children()
        {
            return SubmodulesAndJobs;
        }

        public string DebugID { get { return (Parent?.DebugID ?? "") + "#" + Name; } }
    }

    public class UISubmodule : UIBase
    {
        public UIProject Repository { get { return fRepository; } set { fRepository = value; NotifyPropertyChanged("Repository"); } }
        private UIProject fRepository;
        public bool IsOnHead { get; set; }
        public bool IsValid { get; set; }
        public string HeadCommit { get; set; }
        public string Path { get; set; }

        public UIBranch Parent { get; set; }
        //public ObservableCollection<UISubmodule> Submodules { get; set; } = new ObservableCollection<UISubmodule>();
        public override void Clear()
        {
            Parent = null;
            //Submodules.Clear();
        }
        public override List<UIBase> Children()
        {
            return new List<UIBase>();
        }

        public string DebugID { get { return (Parent?.DebugID ?? "") + "$" + Path; } }
    }

    public class UIJob : UIBase
    {
        public UIBranch Parent { get; set; }
        public override void Clear()
        {
            Parent = null;
        }
        public override List<UIBase> Children()
        {
            return new List<UIBase>();
        }
    }

    public abstract class UIBase : NotifyPropertyBase
    {
        public string Name { get { return fName; } set { fName = value; NotifyPropertyChanged("Name"); } }
        private string fName;
        public bool IsExpanded { get { return fIsExpanded; } set { fIsExpanded = value; NotifyPropertyChanged("IsExpanded"); } }
        private bool fIsExpanded = false;
        public string SubmoduleOKCount { get { return fSubmoduleOKCount; } set { fSubmoduleOKCount = value; NotifyPropertyChanged("SubmoduleOKCount"); } }
        private string fSubmoduleOKCount;
        public int smcOK = 0;
        public string SubmoduleBehindCount { get { return fSubmoduleBehindCount; } set { fSubmoduleBehindCount = value; NotifyPropertyChanged("SubmoduleBehindCount"); } }
        private string fSubmoduleBehindCount;
        public int smcBehind = 0;
        public string SubmoduleInvalidCount { get { return fSubmoduleInvalidCount; } set { fSubmoduleInvalidCount = value; NotifyPropertyChanged("SubmoduleInvalidCount"); } }
        private string fSubmoduleInvalidCount;
        public int smcInvalid = 0;

        public string Status = "";
        public SolidColorBrush StateColor { get { return fStateColor; } set { fStateColor = value; NotifyPropertyChanged("StateColor"); } }
        private SolidColorBrush fStateColor = new SolidColorBrush(Colors.Gray);

        public abstract void Clear();
        public abstract List<UIBase> Children();
        public void ExpandAll()
        {
            IsExpanded = true;
            foreach (var child in Children())
                child.ExpandAll();
        }
        public void CollapseAll()
        {
            IsExpanded = false;
            foreach (var child in Children())
                child.CollapseAll();
        }
    }

    public class NotifyPropertyBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}