﻿using System.IO;
using System.Globalization;

namespace World
{

    public class SimpleIni
    {
        // create a default ini with the same name as the executable but with .ini as extension
        public static SimpleIni Ini = new(Path.ChangeExtension(System.Reflection.Assembly.GetExecutingAssembly().Location, ".ini"));

        public const string DEFAULT_SECTION = "settings";

        public SimpleIni(string aFileName)
        {
            FileName = aFileName;
            // parse ini file into sections and values
            if (File.Exists(aFileName))
            {
                var lines = File.ReadAllLines(aFileName);
                // initial section is unnamed
                var sectionName = "";
                Dictionary<string, string> section = new();
                Sections[sectionName] = section;
                // parse lines
                foreach (var line in lines)
                {
                    var lineTrimmed = line.Trim();
                    if (lineTrimmed.StartsWith('['))
                    {
                        sectionName = lineTrimmed[1..^1].ToLower();
                        if (!Sections.TryGetValue(sectionName, out section))
                        {
                            section = new();
                            Sections[sectionName] = section;
                        }
                    }
                    else if (!string.IsNullOrEmpty(lineTrimmed) && !lineTrimmed.StartsWith(';') && !lineTrimmed.StartsWith('#'))
                    {
                        var p = lineTrimmed.IndexOf('=');
                        var name = lineTrimmed[..p].Trim().ToLower();
                        var value = lineTrimmed[(p + 1)..].Trim();
                        if (!string.IsNullOrEmpty(name))
                            section[name] = value;
                    }
                }
            }
        }

        public string FileName { get; private set; }

        public Dictionary<string, Dictionary<string, string>> Sections = new();

        public bool SectionExists(string aSection)
        {
            return Sections.ContainsKey(aSection.ToLower());
        }

        public bool ValueExists(string aName, string aSection = DEFAULT_SECTION)
        {
            if (Sections.TryGetValue(aSection.ToLower(), out var section))
                return section.ContainsKey(aName.ToLower());
            else
                return false;
        }

        public string Value(string aName, string aDefault = "", string aSection = DEFAULT_SECTION)
        {
            if (Sections.TryGetValue(aSection.ToLower(), out var section))
                return section.TryGetValue(aName.ToLower(), out var value) ? value : aDefault;
            else
                return aDefault;
        }

        public int Value(string aName, int aDefault, string aSection = DEFAULT_SECTION)
        {
            if (Sections.TryGetValue(aSection.ToLower(), out var section))
            {
                if (section.TryGetValue(aName.ToLower(), out var value))
                    return int.TryParse(value, out var intValue) ? intValue : aDefault;
                else
                    return aDefault;
            }
            else
                return aDefault;
        }

        public bool Value(string aName, bool aDefault, string aSection = DEFAULT_SECTION)
        {
            if (Sections.TryGetValue(aSection.ToLower(), out var section))
            {
                if (section.TryGetValue(aName.ToLower(), out var value))
                    return value != "0" && value.ToLower() != "false";
                else
                    return aDefault;
            }
            else
                return aDefault;
        }

        public double Value(string aName, double aDefault, string aSection = DEFAULT_SECTION)
        {
            if (Sections.TryGetValue(aSection.ToLower(), out var section))
            {
                if (section.TryGetValue(aName.ToLower(), out var value))
                    return double.TryParse(value, CultureInfo.InvariantCulture, out var doubleValue) ? doubleValue : aDefault;
                else
                    return aDefault;
            }
            else
                return aDefault;
        }
    }
}